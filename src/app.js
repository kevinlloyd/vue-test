
(function() {
    var http = axios.create({
        baseURL: 'https://restcountries.eu/rest/v2'
    });
    var app = new Vue({
        el: '#app',
        data: {
            query: '',
            results: [],
        },
        methods: {
            search: function(e) {
                e.preventDefault();

                var url = '/name/' + this.query;
                http.get(url)
                    .then(function (response) {
                        console.log(response);
                        app.results = response.data;
                    })
                    .catch(function (error) {
                        console.log(error);
                        app.results = [];
                    });
            }
        }
    })
})();
